highcharts本身已经是封装的很强大的插件，在官网有完整的文档可根据具体的需求进行阅读。在该文件夹会有一个demo供参考

一些常见的设置：
1、去除highcharts的logo水印
credits: {
    enabled: false//将这个参数设置成false即可
}

2、颜色设置：
colors: ['#7cb5ec', '#434348', '#90ed7d', '#f7a35c', '#8085e9', 
    '#f15c80', '#e4d354', '#8085e8', '#8d4653', '#91e8e1'] 

