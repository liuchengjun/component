
一、可把index.html直接拿过来用。
<!DOCTYPE HTML>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <title>ueditor demo</title>
</head>

<body>
    <!-- 加载编辑器的容器 -->
    <script id="container" name="content" type="text/plain">
        这里写你的初始化内容
    </script>
    <!-- 配置文件 -->
    <script type="text/javascript" src="ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="ueditor.all.js"></script>
    <!-- 实例化编辑器 -->
    <script type="text/javascript">
        var ue = UE.getEditor('container');
        //对编辑器的操作最好在编辑器ready之后再做
        ue.ready(function() {
            //设置编辑器的内容
            ue.setContent('hello');
            //获取html内容，返回: <p>hello</p>
            var html = ue.getContent();
            //获取纯文本内容，返回: hello
            var txt = ue.getContentTxt();
        });
    </script>
</body>

</html>
二、涉及到一些PHP后台接口设置，否则会报错

三、方法参考index.html里的下面的按钮方法:
获得整个html的内容  getAllHtml()
获得内容  getContent()
写入内容  setContent()
追加内容  setContent(true)
获得纯文本  getContentTxt()
获得带格式的纯文本  getPlainTxt()
判断是否有内容  hasContent()
使编辑器获得焦点  setFocus()
编辑器是否获得焦点  isFocus(event)
编辑器失去焦点  setblur(event)
获得当前选中的文本  getText()
插入给定的内容  insertHtml()
可以编辑  setEnabled()
不可编辑  setDisabled()
隐藏编辑器  UE.getEditor('editor').setHide()
显示编辑器  UE.getEditor('editor').setShow()
设置高度为300默认关闭了自动长高  UE.getEditor('editor').setHeight(300)
获取草稿箱内容  getLocalData()
清空草稿箱  clearLocalData()
创建编辑器  createEditor()
删除编辑器  deleteEditor()

四、官方文档：
http://fex.baidu.com/ueditor/

五、官方API
http://ueditor.baidu.com/doc/

六、官方演示：
http://ueditor.baidu.com/website/onlinedemo.html

